import { AlertController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ApiService } from './../../services/api.service';
import { Persona } from './../../models/persona';
import { Asignacion } from './../../models/asignacion';
import { Vehiculo } from './../../models/vehiculo';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-crear',
  templateUrl: './crear.page.html',
  styleUrls: ['./crear.page.scss'],
})
export class CrearPage implements OnInit {

  personas: Persona[];
  vehiculos: Vehiculo[];
  modelo: Asignacion;
  loader: any;

  /**
   * Constructor de la clase
   * @param apiService Permite el consumo de los servicios
   * @param router Manejador de las navegación
   * @param alertController Manejador de las alertas
   */
  constructor(
    public apiService: ApiService,
    public router: Router,
    public alertController: AlertController,
    public loadingController: LoadingController
  ) {
    this.modelo = new Asignacion();
    this.personas = [];
    this.vehiculos = [];
  }

  ngOnInit() {
    this.cargarPersonas();
    this.cargarVehiculos();
  }

  /**
   * Carga los datos cada vez que ingresemos a la vista
   */
  ionViewWillEnter() {
    this.modelo = new Asignacion();
  }

  /**
   * Encargado de presentar el loader
   */
  async presentarLoader() {
    this.loader = await this.loadingController.create({
      message: 'Cargando...'
    });
    this.loader.present();
  }

  /**
   * Encargado de presentar los errores de validación
   * @param message Mensaje de error
   */
  async presentarError(message: string) {
    const alert = await this.alertController.create({
      header: 'Error',
      message,
      buttons: ['OK']
    });

    await alert.present();
  }

  /**
   * Permite procesar el formulario
   */
  enviarFormulario() {
    this.presentarLoader();
    return this.apiService.crearElemento(this.modelo, 'asignacion').subscribe((response) => {
      this.router.navigate(['/asignacion']);
      this.loader.dismiss();
    }, error => {
      this.loader.dismiss();
      this.presentarError(error);
    });

  }

  /**
   * Permite obtener el listado de personas
   */
  cargarPersonas() {
    return this.apiService.obtenerListado('persona').subscribe(response => {
      console.log(response);
      this.personas = response;
    });
  }

  /**
   * Permite obtener el listado de vehículos
   */
  cargarVehiculos() {
    return this.apiService.obtenerListado('vehiculo').subscribe(response => {
      console.log(response);
      this.vehiculos = response;
    });
  }

  /**
   * Permite cancelar la creación del registro
   */
  async cancelar() {
    const alert = await this.alertController.create({
      header: 'Confirmar operación',
      message: '¿Esta seguro de cancelar la operación?',
      buttons: [
        {
          text: 'Si, cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            this.router.navigate(['/asignacion']);
          }
        }, {
          text: 'No, continuar',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }

}
