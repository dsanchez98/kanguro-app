import { LoadingController } from '@ionic/angular';
import { ApiService } from './../services/api.service';
import { Component, OnInit } from '@angular/core';

/**
 * Clase que reprensenta el listado de asignaciones
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 */
@Component({
  selector: 'app-asignacion',
  templateUrl: './asignacion.page.html',
  styleUrls: ['./asignacion.page.scss'],
})
export class AsignacionPage implements OnInit {

  /**
   * Permite almacenar los elementos a mostrar
   */
  listado: any;

  /**
   * Instancia del loader
   */
  loader: any;

  /**
   * Constructor de la clase
   * @param apiService Encargado del consumo de los servicios
   * @param loadingController Controlador del loader
   */
  constructor(
    public apiService: ApiService,
    public loadingController: LoadingController
  ) {
    this.listado = [];
  }

  ngOnInit() { }

  /**
   * Carga los datos cada vez que ingresemos a la vista
   */
  ionViewWillEnter() {
    this.obtenerDatos();
  }

  /**
   * Presenta el loader mientras se consume el servicio
   */
  async presentarLoader() {
    this.loader = await this.loadingController.create({
      message: 'Cargando...'
    });

    this.loader.present();

  }

  /**
   * Permite la carga de los datos
   */
  obtenerDatos() {
    this.presentarLoader();
    // Permite obtener el listado de asignaciones
    this.apiService.obtenerListado('asignacion').subscribe(response => {
      console.log(response);
      this.listado = response;
      this.loader.dismiss();
    }, error => {
      this.loader.dismiss();
    });
  }

  /**
   * Permite eliminar un elemento
   * @param elemento 
   */
  eliminar(elemento) {
    this.presentarLoader();
    // Eliminamos la asignacion
    this.apiService.eliminarElemento(elemento.asignacion_id, 'asignacion')
      .subscribe(Response => {
        this.loader.dismiss();
        // Actualizamos el listado
        this.obtenerDatos();
      }, error => {
        this.loader.dismiss();
      });
  }

}
