import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { LoadingController } from '@ionic/angular';

/**
 * Clase que reprensenta el listado de personas
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 */
@Component({
  selector: 'app-personas',
  templateUrl: './personas.page.html',
  styleUrls: ['./personas.page.scss'],
})
export class PersonasPage implements OnInit {

  /**
   * Permite almacenar los elementos a mostrar
   */
  listado: any;

  /**
   * Instancia del loader
   */
  loader: any;

  /**
   * Constructor de la clase
   * @param apiService Encargado del consumo de los servicios
   * @param loadingController Controlador del loader
   */
  constructor(
    public apiService: ApiService,
    public loadingController: LoadingController
  ) {
    this.listado = [];
  }

  ngOnInit() { }

  /**
   * Carga los datos cada vez que ingresemos a la vista
   */
  ionViewWillEnter() {
    this.obtenerDatos();
  }

  /**
   * Presenta el loader mientras se consume el servicio
   */
  async presentarLoader() {
    this.loader = await this.loadingController.create({
      message: 'Cargando...'
    });

    this.loader.present();

  }

  /**
   * Permite la carga de los datos
   */
  obtenerDatos() {
    this.presentarLoader();
    // Permite obtener el listado de personas
    this.apiService.obtenerListado('persona').subscribe(response => {
      console.log(response);
      this.listado = response;
      this.loader.dismiss();
    }, error => {
      this.loader.dismiss();
    });
  }

  /**
   * Permite eliminar un elemento
   * @param elemento 
   */
  eliminar(elemento) {
    // Eliminamos la persona
    this.apiService.eliminarElemento(elemento.persona_id, 'persona').subscribe(Response => {
      // Actualizamos el listado
      this.obtenerDatos();
    });
  }

}
