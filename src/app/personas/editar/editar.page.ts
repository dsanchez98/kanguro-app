import { LoadingController } from '@ionic/angular';
import { Ciudad } from './../../models/ciudad';
import { ApiService } from './../../services/api.service';
import { Persona } from './../../models/persona';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

/**
 * Clase que reprensenta la edición de una persona
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 */
@Component({
  selector: 'app-editar',
  templateUrl: './editar.page.html',
  styleUrls: ['./editar.page.scss'],
})
export class EditarPage implements OnInit {

  /**
   * Identificador de la persona
   */
  id: number;

  modelo: Persona;
  ciudades: Ciudad[];
  loader: any;

  /**
   * Constructor de la clase
   * @param activatedRoute 
   * @param router 
   * @param apiService 
   * @param loadingController 
   * @param alertController 
   */
  constructor(
    public activatedRoute: ActivatedRoute,
    public router: Router,
    public apiService: ApiService,
    public loadingController: LoadingController,
    public alertController: AlertController
  ) {
    this.modelo = new Persona();
    this.ciudades = [];
  }

  /**
   * Encargado de presentar los errores de validación
   * @param message Mensaje de error
   */
  async presentarError(message: string) {
    const alert = await this.alertController.create({
      header: 'Error',
      message,
      buttons: ['OK']
    });

    await alert.present();
  }


  /**
   * Encargado de presentar el loader
   */
  async presentarLoader() {
    this.loader = await this.loadingController.create({
      message: 'Cargando...'
    });
    this.loader.present();
  }

  async ngOnInit() {

    this.presentarLoader();

    await this.cargarCiudades();

    this.id = this.activatedRoute.snapshot.params["id"];
    this.apiService.obtenerElemento(this.id, 'persona').subscribe(response => {
      console.log(response);
      this.modelo = response;
      this.loader.dismiss();
    }, error => {
      this.loader.dismiss();
    });


  }

  actualizar() {
    this.presentarLoader();

    this.apiService.actualizarElemento(this.id, this.modelo, 'persona').subscribe(response => {
      this.router.navigate(['/personas']);
      this.loader.dismiss();
    }, error => {
      this.loader.dismiss();
      this.presentarError(error);
    });
  }

  cargarCiudades() {
    // Permite obtener el listado de ciudades
    return this.apiService.obtenerListado('ciudad').subscribe(response => {
      console.log(response);
      this.ciudades = response;
      this.loader.dismiss();
    }, error => {
      this.loader.dismiss();
    });
  }

  /**
   * Permite cancelar la creación del registro
   */
  async cancelar() {
    const alert = await this.alertController.create({
      header: 'Confirmar operación',
      message: '¿Esta seguro de cancelar la operación?',
      buttons: [
        {
          text: 'Si, cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            this.router.navigate(['/personas']);
          }
        }, {
          text: 'No, continuar',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }


}
