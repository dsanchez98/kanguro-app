import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { PersonasPageModule } from '../personas/personas.module';
import { VehiculosPageModule } from '../vehiculos/vehiculos.module';
import { AsignacionPageModule } from '../asignacion/asignacion.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      },
      { path: 'vehiculos', component: VehiculosPageModule },
      { path: 'personas', component: PersonasPageModule },
      { path: 'asignaciones', component: AsignacionPageModule },
    ])
  ],
  declarations: [HomePage]
})
export class HomePageModule {}
