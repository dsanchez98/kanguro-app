import { LoadingController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { Component } from '@angular/core';
import { DatosInicio } from '../models/datos-inicio';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  loader: any;
  datos: DatosInicio[] = [];

  constructor(
    public apiService: ApiService,
    public loadingController: LoadingController
  ) {
  }

  ionViewWillEnter() {
    this.cargarDatos();
  }

  async presentarLoader() {
    this.loader = await this.loadingController.create({
      message: 'Cargando...'
    });

    this.loader.present();
  }

  /**
   * Permite cargar los datos del panel principal
   */
  cargarDatos() {

    this.presentarLoader();

    // Permite obtener el listado de personas
    this.apiService.obtenerDatosInicio()
      .subscribe(response => {
        console.log(response);

        this.datos = [];
        const vehiculos = new DatosInicio();
        vehiculos.icono = 'car';
        vehiculos.titulo = 'Vehículos';
        vehiculos.cantidad = response.vehiculos || 0;
        vehiculos.ruta = ['/vehiculos'];

        this.datos.push(vehiculos);

        const personas = new DatosInicio();
        personas.icono = 'people';
        personas.titulo = 'Personas';
        personas.cantidad = response.personas || 0;
        personas.ruta = ['/personas'];

        this.datos.push(personas);

        const asignaciones = new DatosInicio();
        asignaciones.icono = 'clipboard';
        asignaciones.titulo = 'Asignaciones';
        asignaciones.cantidad = response.asignaciones || 0;
        asignaciones.ruta = ['/asignacion'];

        this.datos.push(asignaciones);

        this.loader.dismiss();
      }, error => {
        this.loader.dismiss();
      });

  }

}
