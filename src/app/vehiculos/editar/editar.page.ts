import { Persona } from './../../models/persona';
import { LoadingController, AlertController } from '@ionic/angular';
import { ApiService } from './../../services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Vehiculo } from './../../models/vehiculo';
import { Component, OnInit } from '@angular/core';

/**
 * Clase que reprensenta la edición de una persona
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 */
@Component({
  selector: 'app-editar',
  templateUrl: './editar.page.html',
  styleUrls: ['./editar.page.scss'],
})
export class EditarPage implements OnInit {

  /**
   * Identificador de la persona
   */
  id: number;

  tipos: any[];
  personas: Persona[];
  modelo: Vehiculo;
  loader: any;

  /**
   * Constructor de la clase
   * @param activatedRoute 
   * @param router 
   * @param apiService 
   * @param loadingController 
   * @param alertController 
   */
  constructor(
    public activatedRoute: ActivatedRoute,
    public router: Router,
    public apiService: ApiService,
    public loadingController: LoadingController,
    public alertController: AlertController
  ) {
    this.modelo = new Vehiculo();
    this.tipos = [
      {
        tipo_id: 'PAR',
        nombre: 'Particular'
      },
      {
        tipo_id: 'PUB',
        nombre: 'Público'
      },
    ];
    this.personas = [];
  }

  /**
   * Encargado de presentar los errores de validación
   * @param message Mensaje de error
   */
  async presentarError(message: string) {
    const alert = await this.alertController.create({
      header: 'Error',
      message,
      buttons: ['OK']
    });

    await alert.present();
  }


  /**
   * Encargado de presentar el loader
   */
  async presentarLoader() {
    this.loader = await this.loadingController.create({
      message: 'Cargando...'
    });
    this.loader.present();
  }

  async ngOnInit() {

    this.presentarLoader();

    await this.cargarPersonas();

    this.id = this.activatedRoute.snapshot.params["id"];
    this.apiService.obtenerElemento(this.id, 'vehiculo').subscribe(response => {
      console.log(response);
      this.modelo = response;
      this.loader.dismiss();
    }, error => {
      this.loader.dismiss();
    });


  }

  /**
   * Permite procesar los datos del formulario
   */
  actualizar() {
    this.presentarLoader();

    this.apiService.actualizarElemento(this.id, this.modelo, 'vehiculo').subscribe(response => {
      this.router.navigate(['/vehiculos']);
      this.loader.dismiss();
    }, error => {
      this.loader.dismiss();
      this.presentarError(error);
    });
  }

  /**
   * Permite obtener el listado de personas
   */
  cargarPersonas() {
    return this.apiService.obtenerListado('persona').subscribe(response => {
      console.log(response);
      this.personas = response;
    });
  }

  /**
   * Permite cancelar la creación del registro
   */
  async cancelar() {
    const alert = await this.alertController.create({
      header: 'Confirmar operación',
      message: '¿Esta seguro de cancelar la operación?',
      buttons: [
        {
          text: 'Si, cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            this.router.navigate(['/vehiculos']);
          }
        }, {
          text: 'No, continuar',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }


}
