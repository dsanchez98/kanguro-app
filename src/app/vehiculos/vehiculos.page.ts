import { LoadingController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { Component, OnInit } from '@angular/core';

/**
 * Clase que reprensenta el listado de vehículos
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 */
@Component({
  selector: 'app-vehiculos',
  templateUrl: './vehiculos.page.html',
  styleUrls: ['./vehiculos.page.scss'],
})
export class VehiculosPage implements OnInit {

  /**
   * Permite almacenar los elementos a mostrar
   */
  listado: any;

  /**
   * Instancia del loader
   */
  loader: any;

  /**
   * Constructor de la clase
   * @param apiService Encargado del consumo de los servicios
   * @param loadingController Controlador del loader
   */
  constructor(
    public apiService: ApiService,
    public loadingController: LoadingController
  ) {
    this.listado = [];
  }

  ngOnInit() { }

  /**
   * Carga los datos cada vez que ingresemos a la vista
   */
  ionViewWillEnter() {
    this.obtenerDatos();
  }

  /**
   * Presenta el loader mientras se consume el servicio
   */
  async presentarLoader() {
    this.loader = await this.loadingController.create({
      message: 'Cargando...'
    });

    this.loader.present();

  }

  /**
   * Permite la carga de los datos
   */
  obtenerDatos() {
    this.presentarLoader();
    // Permite obtener el listado de vehiculos
    this.apiService.obtenerListado('vehiculo').subscribe(response => {
      console.log(response);
      this.listado = response;
      this.loader.dismiss();
    }, error => {
      this.loader.dismiss();
    });
  }

  /**
   * Permite eliminar un elemento
   * @param elemento 
   */
  eliminar(elemento) {
    // Eliminamos la vehiculo
    this.apiService.eliminarElemento(elemento.vehiculo_id, 'vehiculo').subscribe(Response => {
      // Actualizamos el listado
      this.obtenerDatos();
    });
  }

}
