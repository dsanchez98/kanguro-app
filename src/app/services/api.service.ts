import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  // API path
  urlBaseRestApi = 'http://www.kanguro.ml';


  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer O8IK2MRarQTr9qzgbpcOdn88-dmc4afQ',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': '*',
    })
  };

  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);

      if (error.status === 422) {

        let errorHtml = '<li>';

        error.error.forEach(error => {
          errorHtml += '<ul>' + error.message + '</ul>';
        });

        errorHtml += '</li>';

        return throwError(errorHtml);
      }

    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }

  /**
   * Permite la creacion de un nuevo elemento
   * @param elemento any
   * @param tipo string
   */
  crearElemento(elemento, tipo): Observable<any> {
    return this.http
      .post<any>(this.urlBaseRestApi + '/' + tipo, JSON.stringify(elemento), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      );
  }

  // Permite obtener un elemento
  obtenerElemento(id, tipo): Observable<any> {
    return this.http
      .get<any>(this.urlBaseRestApi + '/' + tipo + '/' + id, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      );
  }

  // Get data
  obtenerListado(tipo): Observable<any> {
    return this.http
      .get<any>(this.urlBaseRestApi + '/' + tipo, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      );
  }

  // Update item by id
  actualizarElemento(id, item, tipo): Observable<any> {
    return this.http
      .put<any>(this.urlBaseRestApi + '/' + tipo + '/' + id, JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      );
  }

  // Delete item by id
  eliminarElemento(id, tipo) {
    return this.http
      .delete<any>(this.urlBaseRestApi + '/' + tipo + '/' + id, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      );
  }

  obtenerDatosInicio(): Observable<any> {
    return this.http
      .get<any>(this.urlBaseRestApi + '/site/home', this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      );
  }

}
