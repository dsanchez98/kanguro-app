import { Persona } from './persona';

/**
 * Definición de la clase Vehículo
 */
export class Vehiculo {
    vehiculo_id: number;
    placa: string;
    color: string;
    marca: string;
    tipo_vehiculo: string;
    propietario_id: number;
    propietario: Persona;
}
