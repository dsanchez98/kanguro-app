import { Persona } from './persona';
import { Vehiculo } from './vehiculo';

/**
 * Definición de la clase Asignación
 */
export class Asignacion {
   asignacion_id: number;
   conductor_id: number;
   conductor: Persona;
   vehiculo_id: number;
   vehiculo: Vehiculo;
}
