import { Ciudad } from './ciudad';

/**
 * Definición de la clase Persona
 */
export class Persona {

    persona_id: number;
    numero_documento: string;
    primer_nombre: string;
    segundo_nombre: string;
    apellidos: string;
    direccion: string;
    telefono: string;
    ciudad: Ciudad;
    ciudad_id: number;

}
