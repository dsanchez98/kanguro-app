/**
 * Definición de la clase DatosInicio
 */
export class DatosInicio {
    icono: string;
    titulo: string;
    cantidad: number;
    ruta: any;
}
